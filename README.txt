DESCRIPTION
Let registered users invite others to visit your site.

Site owners may pass custom text onto the invite form by linking to it with simpleinvite_custom=<foo> on the querystring. That will become the user's default custom text. If you want to always do this and never show the built in body text on your invite emails, you can set $conf['simpleinvite_page_body] = '' in your settings.php

MAINTAINER
-----------
Moshe Weitzman <weitzman AT tejasa DOT com>